#include "FindMaximumCliques.h"

#include <fmt/format.h>
#include <pystring/pystring.h>

#include <algorithm>
#include <chrono>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <iterator>
#include <stdexcept>
#include <string>
#include <unordered_set>

namespace fs = std::filesystem;

static std::vector<fs::path> ListWordFiles(const fs::path words_dir)
{
	fs::directory_iterator dir_iter{ words_dir };
	std::vector<fs::path> word_files;
	std::copy_if(fs::begin(dir_iter), fs::end(dir_iter), std::back_inserter(word_files), [](auto dir_entry) { return dir_entry.is_regular_file(); });
	return word_files;
}

static fs::path SelectWordsFile(const fs::path words_dir)
{
	auto wordfiles = ListWordFiles(words_dir);
	fmt::print("Select a words file:\n\n");
	int counter = 1;
	for (fs::path wordfile : wordfiles)
	{
		fmt::print("{:<4} {}\n", counter, wordfile.filename().string());
		counter += 1;
	}
	fmt::print("\nSelection: ");
	int selection;
	std::cin >> selection;
	fmt::print("\n");
	if (selection < 1 || selection > wordfiles.size())
		return "";
	return wordfiles[selection - 1];
}

static bool HasInvalidChar(const std::string word)
{
    for (char ch : word)
    {
        if (ch < 'a' || ch > 'z')
            return true;
    }
    return false;
}

static bool AllLettersUnique(const std::string& word)
{
    int Seen['z' - 'a' + 1] = { 0 }; // Initializes all elements to zero
    for (char ch : word)
    {
        if (Seen[ch - 'a'])
            return false;
        Seen[ch -'a'] = 1;
    }
	return true;
}

static bool AllLettersUnique(const std::string& word1, const std::string& word2)
{
	int Seen[26] = { 0 }; // Initializes all elements to zero
	for (char ch : word1)
	{
		if (Seen[ch - 'a'])
			return false;
		Seen[ch - 'a'] = 1;
	}
	for (char ch : word2)
	{
		if (Seen[ch - 'a'])
			return false;
		Seen[ch - 'a'] = 1;
	}
	return true;
}


std::vector<std::string> ReadWords(const std::string& filename)
{
    std::unordered_set<std::string> wordsset;
    std::ifstream ifs(filename);
	if (!ifs.good())
		throw std::runtime_error(fmt::format("Could not read file {}", filename));
	int count_all = 0;
	int count_having_five_letters = 0;
	int count_having_valid_letters = 0;
	int count_having_unique_letters = 0;
	for (std::string line; std::getline(ifs, line); )
    {
        std::string word = pystring::strip(line);
		if (word.empty())
			continue;
		count_all += 1;
		if (word.size() != 5)
            continue;
		count_having_five_letters += 1;
        if (HasInvalidChar(word))
            continue;
		count_having_valid_letters += 1;
        if (!AllLettersUnique(word))
            continue;
		count_having_unique_letters += 1;
        wordsset.insert(word);
    }
	std::vector<std::string> wordslist(wordsset.begin(), wordsset.end());
	std::sort(wordslist.begin(), wordslist.end());
	fmt::print("    all words:                      {}\n", count_all);
	fmt::print("    words having 5 letters:         {}\n", count_having_five_letters);
	fmt::print("    words having all valid letters: {}\n", count_having_valid_letters);
	fmt::print("    words having unique letters:    {}\n", count_having_unique_letters);
	fmt::print("    words effective:                {}\n", wordslist.size());
    return wordslist;
}

fmc::Graph Build_5_5_25_Graph(const std::vector<std::string>& words)
{
    fmc::Graph graph;
    for (size_t i = 0; i + 1 < words.size(); ++i)
    {
		graph.AddVertex(words[i]);
        for (size_t j = i + 1; j < words.size(); ++j)
        {
            if (AllLettersUnique(words[i], words[j]))
                graph.AddEdge(words[i], words[j]);
        }
    }
    return graph;
}

struct CollectMaximumCliques
{
	CollectMaximumCliques(const fs::path results_path)
		: results_path(results_path)
	{
		// Create directory for results file if needed
		fs::create_directories(results_path.parent_path());
		// Truncate existing file, if any
		std::ofstream ofs(results_path, std::ios::trunc);
		if (!ofs.is_open())
			throw std::runtime_error(fmt::format("Failed to write file {}", results_path.string()));
		ofs.close();
	}

    void operator()(const fmc::LabelList& max_clique)
    {
		if (max_clique.size() != 5)
			return;

		results.push_back(max_clique);

		std::string formatted_words = pystring::join(" ", max_clique);
		fmt::print("{:<5} {}\n", results.size(), formatted_words);
		std::ofstream ofs(results_path, std::ios::app);
		ofs << formatted_words << "\n";
		ofs.close();
    }

	std::vector<std::vector<std::string>> results;
	fs::path results_path;
};

template<typename Duration> double DurationSecondsAsDouble(const Duration& duration)
{
	return std::chrono::duration<double>(duration).count();
}

static std::pair<std::string, std::string> split_filename_ext(const std::string& filename)
{
	std::vector<std::string> parts;
	pystring::split(filename, parts, ".", 1);
	return std::make_pair(parts[0], parts[1]);
}

void Run(const fs::path& words_path, const fs::path& results_path)
{
	auto started_at = std::chrono::steady_clock::now();

	fmt::print("Read words file {} ...\n", words_path.string());
	auto const words = ReadWords(words_path.string());

	fmt::print("Build graph ...\n");
	fmc::Graph graph = Build_5_5_25_Graph(words);
	auto graph_read_at = std::chrono::steady_clock::now();
	auto dur = graph_read_at - started_at;
	fmt::print("Build graph:   {:.3f} seconds\n", DurationSecondsAsDouble(graph_read_at - started_at));
	fmt::print("\n");

	fmt::print("Find 5x5x25s (write results to {}) ...\n", results_path.string());
	CollectMaximumCliques collect_maximum_cliques(results_path);
	//fmc::BronkerBosch_WithoutPivoting(graph, collect_maximum_cliques);
	fmc::BronkerBosch_WithPivoting(graph, collect_maximum_cliques);
	auto finished_at = std::chrono::steady_clock::now();
	fmt::print("\n");
	fmt::print("Found {} 5x5x25s\n", collect_maximum_cliques.results.size());
	fmt::print("Results written to {}\n", results_path.string());
	fmt::print("Find 5x5x25s:  {:.3f} seconds\n", DurationSecondsAsDouble(finished_at - graph_read_at));
	fmt::print("Total time:    {:.3f} seconds\n", DurationSecondsAsDouble(finished_at - started_at));
}

const fs::path words_dir = fs::path("data") / "wordfiles";
const fs::path results_dir = fs::path("data") / "results";

int main(int argc, const char *argv[])
{
	try
	{
		fs::path words_path;
		if (argc > 1)
			words_path = fs::path(argv[1]);
		else
			words_path = SelectWordsFile(words_dir);
		if (words_path.string().empty())
			throw std::runtime_error("No words file specified");
		fs::path results_path = results_dir / words_path.filename().replace_extension(".txt");
		Run(words_path, results_path);
	}
	catch (std::exception& e)
	{
		fmt::print("Exception: {}\n", e.what());
	}

    return 0;
}
