#pragma once

#include <algorithm>
#include <iterator>
#include <limits>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>

namespace fmc
{

    using Label = std::string;
    using LabelList = std::vector<Label>;
    using Index = int; // Index into internal vector of vertices
	using IndexList = std::vector<Index>;
    using IndexSet = std::unordered_set<Index>; // or maybe vector is faster in our case, since we deal with low numbers of neigbors
    struct Vertex
    {
        Label label;
        Index index;
        IndexSet neighbors;

        Vertex(const Label& label)
            : label(label)
            , index(-1)
        {
        }
    };
    using VertexList = std::vector<Vertex>;

    class Graph
    {
    public:
        Graph()
        {
        }

        // Copy constructor
        Graph(const Graph& other)
            : m_VertexList(other.m_VertexList)
            , m_LabelToIndexMap(other.m_LabelToIndexMap)
        {
        }

        // Copy assignment operator
        Graph& operator=(const Graph& other)
        {
            m_VertexList = other.m_VertexList;
            m_LabelToIndexMap = other.m_LabelToIndexMap;
            return *this;
        }

        // Move constructor
        Graph(const Graph&& other)
            : m_VertexList(std::move(other.m_VertexList))
            , m_LabelToIndexMap(std::move(other.m_LabelToIndexMap))
        {
        }

        // Move assignment operator
        Graph& operator=(const Graph&& other)
        {
            if (this != &other)
            {
                m_VertexList = std::move(other.m_VertexList);
                m_LabelToIndexMap = std::move(other.m_LabelToIndexMap);
            }
            return *this;
        }

		void AddVertex(const Label& label)
		{
			GetOrAddVertex(label);
		}

        void AddEdge(const Label& label_a, const Label& label_b)
        {
            Index index_a = GetOrAddVertex(label_a);
            Index index_b = GetOrAddVertex(label_b);
            m_VertexList[index_a].neighbors.insert(index_b);
            m_VertexList[index_b].neighbors.insert(index_a);
        }

        const Vertex& GetVertex(Index index) const
        {
            return m_VertexList[index];
        }

        const Vertex& GetVertex(const Label& label) const
        {
            return m_VertexList[m_LabelToIndexMap.find(label)->second];
        }

        bool Connected(Index index_a, Index index_b) const
        {
            return GetVertex(index_a).neighbors.count(index_b);
        }

        // Get range of indices: begin, end where begin is inclusive and end is exclusive
        std::pair<Index, Index> GetIndexRange() const
        {
            Index size = static_cast<Index>(m_VertexList.size());
            return std::pair<Index, Index>(0, size);
        }

        template<typename IndexContainer> LabelList GetLabelsFromIndices(const IndexContainer& indices) const
        {
            LabelList label_list;
            std::transform(
				indices.begin(),
				indices.end(),
                std::back_inserter(label_list),
                [&](Index index) { return this->GetVertex(index).label; }
            );
            return label_list;
        }

    private:
        using LabelToIndexMap = std::unordered_map<Label, Index>;

        VertexList m_VertexList;
        LabelToIndexMap m_LabelToIndexMap;

        Index GetOrAddVertex(const Label& label)
        {
            auto map_it = m_LabelToIndexMap.find(label);
            if (map_it == m_LabelToIndexMap.end())
            {
                m_VertexList.push_back(Vertex(label));
                Vertex& vertex = m_VertexList.back();
                vertex.index = static_cast<Index>(m_VertexList.size()) - 1;
                const auto [it, success] = m_LabelToIndexMap.insert({ label, vertex.index });
                map_it = it;
            }
            return map_it->second;
        }
    };

    inline IndexList InitFromRange(const std::pair<Index, Index>& IndexRange)
    {
        IndexList result;
        for (Index i = IndexRange.first; i < IndexRange.second; ++i)
            result.push_back(i);
        return result;
    }

    inline IndexSet operator+(const IndexSet& set_in, const Index& index)
    {
        IndexSet set_out = set_in;
        set_out.insert(index);
        return set_out;
    }

    inline IndexSet Intersection(IndexSet::const_iterator begin1, IndexSet::const_iterator end1, const IndexSet& set2)
    {
        IndexSet set_out;
        for (auto it = begin1; it != end1; ++it)
            if (set2.count(*it))
                set_out.insert(*it);
        return set_out;
    }

    inline IndexSet Intersection(const IndexSet& set1, const IndexSet& set2)
    {
        return Intersection(set1.begin(), set1.end(), set2);
    }

	inline IndexList Intersection(IndexList::const_iterator begin1, IndexList::const_iterator end1, const IndexSet& in2)
	{
		IndexList out;
		for (auto it = begin1; it != end1; ++it)
			if (in2.count(*it))
				out.push_back(*it);
		return out;
	}

	inline IndexList Intersection(const IndexList& in1, const IndexSet& in2)
	{
		return Intersection(in1.begin(), in1.end(), in2);
	}

	//
	// Bronker-Bosch algorithm - the simple version without pivoting
	// Adapted from https://en.wikipedia.org/wiki/Bron%E2%80%93Kerbosch_algorithm#Without_pivoting
	// Changed a bit, mainly because the algorithm as presented there changes P while iterating
	// over it. That's never good.
	// 

    template<typename ReportMaximumClique> void BronkerBosch_WithoutPivoting_Impl(const Graph& graph, ReportMaximumClique& report_maximum_clique, IndexList& R, const IndexList& P, IndexList X)
    {
        if (P.empty() && X.empty())
        {
            report_maximum_clique(graph.GetLabelsFromIndices(R));
        }
        for (IndexList::const_iterator it = P.begin(); it != P.end(); ++it)
        {
			// Instead of making a whole now R each time, it turns out to be significantly faster
			// to add *it to it before the recursive call, and erase *it after the call to restore
			// R to its original value.
			// Can we do something similar for P and/or X? Doesn't look like it.
			R.push_back(*it);
            IndexList Pnew = Intersection(it, P.end(), graph.GetVertex(*it).neighbors);
            IndexList Xnew = Intersection(X, graph.GetVertex(*it).neighbors);
            BronkerBosch_WithoutPivoting_Impl(graph, report_maximum_clique, R, Pnew, Xnew);
			R.pop_back();
            X.push_back(*it);
        }
    }

    template<typename ReportMaximumClique> void BronkerBosch_WithoutPivoting(const Graph& graph, ReportMaximumClique& report_maximum_clique)
    {
        IndexList R;
        IndexList P = InitFromRange(graph.GetIndexRange());
        IndexList X; 
        BronkerBosch_WithoutPivoting_Impl(graph, report_maximum_clique, R, P, X);
    }

	//
	// Bronker-Bosch algorithm - with pivoting
	// Adapted from https://en.wikipedia.org/wiki/Bron%E2%80%93Kerbosch_algorithm#With_pivoting
	// For the pivot selection I read and tried to understand Bronker and Bosch's original article:
	// https://dl.acm.org/doi/pdf/10.1145/362342.362367
	//

	inline int CountDisconnected(const Graph& graph, const IndexList& P, Index index1)
	{
		int count = 0;
		for (auto index2 : P)
		{
			if (index2 != index1 && !graph.Connected(index1, index2))
				count += 1;
		}
		return count;
	}

	inline Index ChoosePivot(const Graph& graph, const IndexList& P, const IndexList& X)
	{
		int smallest_count = std::numeric_limits<int>::max();
		Index smallest_count_index = -1;
		for (Index index : P)
		{
			int count = CountDisconnected(graph, P, index);
			if (count < smallest_count)
			{
				smallest_count = count;
				smallest_count_index = index;
			}
		}
		for (Index index : P)
		{
			int count = CountDisconnected(graph, P, index);
			if (count < smallest_count)
			{
				smallest_count = count;
				smallest_count_index = index;
			}
		}
		if (smallest_count_index < 0)
			return 0; // ?????
		return smallest_count_index;
	}

	template<typename ReportMaximumClique> void BronkerBosch_WithPivoting_Impl(const Graph& graph, ReportMaximumClique& report_maximum_clique, IndexList& R, const IndexList& P, IndexList X)
	{
		if (P.empty() && X.empty())
		{
			report_maximum_clique(graph.GetLabelsFromIndices(R));
		}
		// choose a pivot vertex u in union(P, X)
		Index u = ChoosePivot(graph, P, X);
		const Vertex& vu = graph.GetVertex(u);
		const IndexSet& vun = vu.neighbors;
		// for each vertex v in P \ u.neighbors
		for (IndexList::const_iterator it = P.begin(); it != P.end(); ++it)
		{
			if (vun.count(*it) > 0)
				continue;
			// Instead of making a whole now R each time, it turns out to be significantly faster
			// to add *it to it before the recursive call, and erase *it after the call to restore
			// R to its original value.
			// Can we do something similar for P and/or X? Doesn't look like it.
			R.push_back(*it);
			IndexList Pnew = Intersection(it, P.end(), graph.GetVertex(*it).neighbors);
			IndexList Xnew = Intersection(X, graph.GetVertex(*it).neighbors);
			BronkerBosch_WithoutPivoting_Impl(graph, report_maximum_clique, R, Pnew, Xnew);
			R.pop_back();
			X.push_back(*it);
		}
	}

	template<typename ReportMaximumClique> void BronkerBosch_WithPivoting(const Graph& graph, ReportMaximumClique& report_maximum_clique)
	{
		IndexList R;
		IndexList P = InitFromRange(graph.GetIndexRange());
		IndexList X;
		BronkerBosch_WithPivoting_Impl(graph, report_maximum_clique, R, P, X);
	}

}
